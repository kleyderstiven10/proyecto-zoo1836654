package com.zoo.zoo1836654;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Zoo1836654Application {

	public static void main(String[] args) {
		SpringApplication.run(Zoo1836654Application.class, args);
	}

}
