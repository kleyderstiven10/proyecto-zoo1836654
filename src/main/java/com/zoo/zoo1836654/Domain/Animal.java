package com.zoo.zoo1836654.Domain;

import com.zoo.zoo1836654.Domain.Enumeration.GenderType;


import javax.persistence.*;

@Entity
public class Animal {


    @Id
    private  String code;

    private String name;
    private String race;


    @Enumerated(EnumType.STRING)
    private GenderType genderType;

    @ManyToOne
    private Location location;

    public Animal() {
    }

    public Animal(String code, String name, String race, GenderType genderType, Location location) {
        this.code = code;
        this.name = name;
        this.race = race;
        this.genderType = genderType;
        this.location = location;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public GenderType getGenderType() {
        return genderType;
    }

    public void setGenderType(GenderType genderType) {
        this.genderType = genderType;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}


