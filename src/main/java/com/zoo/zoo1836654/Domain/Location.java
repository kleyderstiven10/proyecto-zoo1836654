package com.zoo.zoo1836654.Domain;



import javax.persistence.*;
@Entity

public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int id;

    private String description;

    @ManyToOne
    private LocationType locationType;

    public Location() {
    }

    public Location(int id, String description, LocationType locationType) {
        this.id = id;
        this.description = description;
        this.locationType = locationType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocationType getLocationType() {
        return locationType;
    }

    public void setLocationType(LocationType locationType) {
        this.locationType = locationType;
    }
}

