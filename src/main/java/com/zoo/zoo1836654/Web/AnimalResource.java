package com.zoo.zoo1836654.Web;

import com.zoo.zoo1836654.Domain.Animal;
import com.zoo.zoo1836654.Repository.dto.AnimalGender;
import com.zoo.zoo1836654.Service.IAnimalService;
import com.zoo.zoo1836654.Repository.dto.AnimalNameGender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@RequestMapping("/api/animal")
public class AnimalResource {

    @Autowired
    IAnimalService animalService;

    @PostMapping("")
    public ResponseEntity create(@RequestBody Animal animal){
        return animalService.create(animal);
    }

    @GetMapping("")
    public Iterable<Animal> read(){
        return animalService.read();
    }

    @PutMapping("")
    public Animal update(@RequestBody Animal animal) {
        return animalService.update(animal);
    }

    @GetMapping("/{id}")
    public Optional<Animal> getById(@PathVariable String code){
        return animalService.getById(code);
    }
    @GetMapping("/count")
    public Integer countAnimal(){
        return animalService.quantityAnimals();
    }
    @GetMapping("/name-gender")
    public Iterable<AnimalNameGender> getAnimalNameGender(){
        return animalService.getAnimalNameGender();
    }
    @GetMapping("/animals-male")
    public Iterable<AnimalGender> getAnimalsMale(){
        return animalService.getAnimalsMale();
    }
    @GetMapping("/animals-female")
    public Iterable<AnimalGender> getAnimalFemale(){
        return animalService.getAnimalsFemale();
    }
    @GetMapping("/count-male")
    public Integer countAnimalMale(){
        return animalService.quantityAnimalsMale();
    }
    @GetMapping("/count-female")
    public Integer countAnimalFemale(){
        return animalService.quantityAnimalsFemale();
    }
}