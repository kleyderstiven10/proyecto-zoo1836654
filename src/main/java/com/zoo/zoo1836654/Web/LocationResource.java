package com.zoo.zoo1836654.Web;

import com.zoo.zoo1836654.Domain.Location;
import com.zoo.zoo1836654.Service.ILocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/location")
public class LocationResource {

    @Autowired
    ILocationService locationService;

    @PostMapping("")
    public ResponseEntity create(@RequestBody Location location){
        return locationService.create(location);
    }

    @GetMapping("")
    public Iterable<Location> read(){
        return locationService.read();
    }

}
