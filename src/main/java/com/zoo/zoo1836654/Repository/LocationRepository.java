package com.zoo.zoo1836654.Repository;
import com.zoo.zoo1836654.Domain.Location;
import org.springframework.data.repository.CrudRepository;

public interface LocationRepository extends CrudRepository<Location, Integer> {



}

