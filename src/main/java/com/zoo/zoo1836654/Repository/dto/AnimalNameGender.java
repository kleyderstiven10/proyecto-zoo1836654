package com.zoo.zoo1836654.Repository.dto;

public interface AnimalNameGender {

    public String getName();
    public String getGender();
}
