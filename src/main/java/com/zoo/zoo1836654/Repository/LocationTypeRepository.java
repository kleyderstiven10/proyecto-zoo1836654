package com.zoo.zoo1836654.Repository;
import com.zoo.zoo1836654.Domain.LocationType;
import org.springframework.data.repository.CrudRepository;

public interface LocationTypeRepository extends CrudRepository<LocationType, Integer>{
}