package com.zoo.zoo1836654.Repository;
import com.zoo.zoo1836654.Domain.Animal;
import com.zoo.zoo1836654.Repository.dto.AnimalGender;
import com.zoo.zoo1836654.Repository.dto.AnimalNameGender;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface AnimalRepository extends CrudRepository<Animal, String> {

    // Cuenta Total De Los Animales
    @Query(value = "select count(animal) from Animal animal")
    Integer quantityAnimals();

    //Lista los animales por nombre
    @Query(value = "select animal.name as name, animal.genderType as genderType from Animal animal")
    Iterable<AnimalNameGender> findAnimalNameGender();

    // Lista de los animales por genero MALE
    @Query(value = "select animal.name as name, animal.genderType as genderType from Animal animal where genderType = 'MALE'")
    Iterable<AnimalGender> findAnimalsMale();

    //Lista de los animales por genero FEMALE
    @Query(value = "select animal.name as genderType from Animal animal where genderType = 'FEMALE'")
    Iterable<AnimalGender> findAnimalsFemale();

    //cuantos  animales machos
    @Query(value = "select count(genderType) from Animal where genderType = 'MALE'")
    Integer quantityAnimalsMale();

    //cuantos  animales hembras
    @Query(value = "select count (genderType) from Animal where genderType = 'FEMALE'")
    Integer quantityAnimalsFemale();


}
