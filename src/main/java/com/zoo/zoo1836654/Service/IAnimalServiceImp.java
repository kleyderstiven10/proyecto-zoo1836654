package com.zoo.zoo1836654.Service;


import com.zoo.zoo1836654.Domain.Animal;
import com.zoo.zoo1836654.Repository.AnimalRepository;
import com.zoo.zoo1836654.Repository.dto.AnimalGender;
import com.zoo.zoo1836654.Repository.dto.AnimalNameGender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IAnimalServiceImp implements IAnimalService {

    @Autowired
    private AnimalRepository animalRepository;

    @Override
    public ResponseEntity create(Animal animal) {

        if(animalRepository.findById(animal.getCode()).isPresent()){
            return new ResponseEntity("This code is used", HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity(animalRepository.save(animal), HttpStatus.OK);
        }
    }

    @Override
    public Iterable<Animal> read() {
        return animalRepository.findAll();
    }

    @Override
    public Animal update(Animal animal) {
        return animalRepository.save(animal);
    }

    @Override
    public Optional<Animal> getById(String code) {
        return animalRepository.findById(code);
    }

    @Override
    public Integer quantityAnimals() {
        return animalRepository.quantityAnimals();
    }

    @Override
    public Iterable<AnimalNameGender> getAnimalNameGender() {
        return animalRepository.findAnimalNameGender();
    }

    @Override
    public Iterable<AnimalGender> getAnimalsMale() {
        return animalRepository.findAnimalsMale();
    }

    @Override
    public Iterable<AnimalGender> getAnimalsFemale() {
        return animalRepository.findAnimalsFemale();
    }

    @Override
    public Integer quantityAnimalsMale() {
        return animalRepository.quantityAnimalsMale();
    }

    @Override
    public Integer quantityAnimalsFemale() {
        return animalRepository.quantityAnimalsFemale();
    }
}
