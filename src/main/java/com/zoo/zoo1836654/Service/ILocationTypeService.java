package com.zoo.zoo1836654.Service;


import com.zoo.zoo1836654.Domain.LocationType;

public interface ILocationTypeService {

    public LocationType create(LocationType locationType);

    public Iterable<LocationType> read();

}
