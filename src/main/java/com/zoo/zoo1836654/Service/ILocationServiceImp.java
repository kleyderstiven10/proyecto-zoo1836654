package com.zoo.zoo1836654.Service;
import com.zoo.zoo1836654.Domain.Location;
import com.zoo.zoo1836654.Repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ILocationServiceImp implements ILocationService{


    @Autowired
    private LocationRepository locationRepository;

    @Override
    public ResponseEntity create(Location location) {
        if(locationRepository.findById(location.getId()).isPresent()){
            return new ResponseEntity("Id Is Use", HttpStatus.BAD_REQUEST);
        }else{
            return new ResponseEntity(locationRepository.save(location), HttpStatus.OK);
        }
    }

    @Override
    public Iterable<Location> read() {
        return locationRepository.findAll();
    }


}