package com.zoo.zoo1836654.Service;
import com.zoo.zoo1836654.Domain.LocationType;
import com.zoo.zoo1836654.Repository.LocationTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




@Service
public class ILocationTypeServiceImp implements ILocationTypeService {

    @Autowired
    private LocationTypeRepository locationTypeRepository;

    @Override
    public LocationType create(LocationType locationType) {
        return locationTypeRepository.save(locationType);

    }

    @Override
    public Iterable<LocationType> read() {
        return locationTypeRepository.findAll();
    }
}
