package com.zoo.zoo1836654.Service;
import com.zoo.zoo1836654.Domain.Animal;
import com.zoo.zoo1836654.Repository.dto.AnimalGender;
import com.zoo.zoo1836654.Repository.dto.AnimalNameGender;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface IAnimalService {

    public ResponseEntity create(Animal animal);

    public Iterable<Animal> read();

    public Animal update (Animal animal);

    public Optional<Animal> getById(String code);

    public Integer quantityAnimals();

    public Iterable<AnimalNameGender> getAnimalNameGender();

    public Iterable<AnimalGender> getAnimalsMale();

    public Iterable<AnimalGender> getAnimalsFemale();

    public Integer quantityAnimalsMale();

    public Integer quantityAnimalsFemale();

}
