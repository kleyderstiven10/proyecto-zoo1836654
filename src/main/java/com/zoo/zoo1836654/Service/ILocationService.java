package com.zoo.zoo1836654.Service;

import com.zoo.zoo1836654.Domain.Location;
import org.springframework.http.ResponseEntity;

public interface ILocationService {

    public ResponseEntity create(Location location);

    public Iterable<Location> read();


}